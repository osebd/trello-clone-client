import { CONSTANTS, requestOptions } from './../actions/index';
import { takeEvery, all, put } from 'redux-saga/effects';
import { storeData } from './../actions/index';

function* manageCard(action) {
    let data
    const { method, ...payload } = action.payload
    requestOptions.body = JSON.stringify(payload)

    yield fetch(process.env.REACT_APP_API_URL + method, requestOptions)
    .then(res => res.json())
    .then(json => data = json)

    yield put(storeData(data))
} 

function* manageCardSaga() {
    yield takeEvery([CONSTANTS.ADD_CARD, CONSTANTS.EDIT_CARD, CONSTANTS.DELETE_CARD], manageCard)
}

export default function* cardSagas() {
    yield all([
        manageCardSaga()
    ])
}