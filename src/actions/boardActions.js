import { CONSTANTS, RPC_METHODS } from './index';

export const addBoard = (boardTitle) => {
    return {
        type: CONSTANTS.ADD_BOARD,
        payload: { boardTitle, method: RPC_METHODS.ADD_BOARD }
    }
}

export const deleteBoard = (boardId) => {
    return {
        type: CONSTANTS.DELETE_BOARD,
        payload: { boardId, method: RPC_METHODS.DELETE_BOARD }
    }
}

export const editBoard = (boardId, boardTitle) => {
    return {
        type: CONSTANTS.EDIT_BOARD,
        payload: { boardId, boardTitle, method: RPC_METHODS.EDIT_BOARD }
    }
}