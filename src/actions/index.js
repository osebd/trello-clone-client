export * from './listActions';
export * from './cardActions';
export * from './boardActions';

export const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
}

export const RPC_METHODS = {
    ADD_BOARD: 'create_board',
    EDIT_BOARD: 'edit_board',
    DELETE_BOARD: 'delete_board',
    ADD_LIST: 'create_list',
    EDIT_LIST: 'edit_list',
    DELETE_LIST: 'delete_list',
    ADD_CARD: 'create_card',
    EDIT_CARD: 'edit_card',
    DELETE_CARD: 'delete_card',
}

export const CONSTANTS = {
    ADD_CARD: 'ADD_CARD',
    ADD_LIST: 'ADD_LIST',
    ADD_BOARD: 'ADD_BOARD',
    DELETE_CARD: 'DELETE_CARD',
    DELETE_LIST: 'DELETE_LIST',
    DELETE_BOARD: 'DELETE_BOARD',
    STORE_DATA: 'STORE_DATA',
    FETCH_DATA: 'FETCH_DATA',
    EDIT_BOARD: 'EDIT_BOARD',
    EDIT_LIST: 'EDIT_LIST',
    EDIT_CARD: 'EDIT_CARD'
};

export const storeData = (data) => {
    return {
        type: CONSTANTS.STORE_DATA,
        payload: data
    }
}

export const fetchData = () => {
    return {
        type: CONSTANTS.FETCH_DATA
    }
}