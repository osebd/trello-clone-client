import { CONSTANTS, requestOptions } from './../actions/index';
import { takeEvery, all, put } from 'redux-saga/effects';
import { storeData } from './../actions/index';

function* manageBoard(action) {
    let data
    const { method, ...payload } = action.payload
    requestOptions.body = JSON.stringify(payload)

    yield fetch(process.env.REACT_APP_API_URL + action.payload.method, requestOptions)
    .then(res => res.json())
    .then(json => data = json)

    yield put(storeData(data))
} 

function* manageBoardSaga() {
    yield takeEvery([CONSTANTS.ADD_BOARD, CONSTANTS.EDIT_BOARD, CONSTANTS.DELETE_BOARD], manageBoard)
}

export default function* boardSagas() {
    yield all([
        manageBoardSaga(),
    ])
}
