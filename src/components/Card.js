import React, { useState } from 'react';
import { Button, Dialog } from '@material-ui/core';
import TextArea from 'react-textarea-autosize';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import DialogTitle from '@material-ui/core/DialogTitle';
import store from './../store/index';
import { connect } from 'react-redux';
import { deleteCard, editCard } from './../actions';
import './../styles/Card.scss';


const SimpleDialog = (props) => {
    const { onClose, content, open } = props;
    const [value, setValue] = useState(content)

    const handleClose = () => {
        onClose(value);
    };

    const handleChange = (e) => {
        setValue(e.target.value)
    }

    return (
        <Dialog onClose={handleClose} className='dialog' open={open}>
                <DialogTitle className='title'>Here you can edit your comment</DialogTitle>
            <List>
                <ListItem>
                    <TextArea 
                        autoFocus
                        value={value}
                        onChange={handleChange}
                        style={{
                            resize: 'none',
                            width: '80%',
                            outline: 'none',
                            border: 'none',
                        }}></TextArea>
                </ListItem>
            </List>
        </Dialog>
    );
}



class Card extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isEditing: false
        }
    }

    handleChange = (event) => {
        this.setState({ value: event.target.value })
    };

    handleEditing = () => {
        this.setState({ isEditing: true })
    }

    handleClose = (value) => {
        store.dispatch(editCard(this.props.cardId, value))
        this.setState({ isEditing: false })
    }



    render() {
        let open = true
        return (
            <div key={this.props.cardId} className='card' onMouseUp={this.handleEditing}>
                <div className='content' onClick={this.handleEditing}>{this.props.content}</div>
                <Button className='delete-btn'
                    variant='contained'
                    color='primary'
                    onMouseDown={() => store.dispatch(deleteCard(this.props.boardId, this.props.listId, this.props.cardId))}>
                    remove
                            </Button>
                <SimpleDialog onClose={this.handleClose} content={this.props.content} open={this.state.isEditing}></SimpleDialog>
            </div>
        )
    }
}

export default connect(null)(Card);