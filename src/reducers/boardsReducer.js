import { CONSTANTS } from '../actions';

const initialState = []

// function addList(state, action) {
//     let newState = []
//     let board = {}

//     const newList = {
//         title: action.payload.title,
//         cards: [],
//     }

//     board = state.find(element => element.id === parseInt(action.payload.boardId))
//     board.lists = [...board.lists, newList]
//     Object.assign(newState, state)
//     newState[newState.indexOf(board)] = board

//     return newState
// }

// function addCard(state, action) {
//     let newState = []
//     let board = {}

//     const newCard = {
//         description: null,
//         content: action.payload.content,
//     }

//     board = state.find(element => element.id === parseInt(action.payload.boardId))
//     const updatedList = board.lists.map(list => {
//         if (list.id === action.payload.listId) {
//             return {
//                 ...list,
//                 cards: [...list.cards, newCard]
//             }
//         }

//         return list;
//     }
//     )

//     Object.assign(newState, state)
//     board.lists = updatedList
//     newState[newState.indexOf(board)] = board

//     return newState;
// }

// function deleteBoard(state, action) {
//     let newState = []

//     const objects = Object.keys(state).map(key => state[key])
//     newState = objects.filter(board => board.id !== action.payload)
//     return newState
// }

// function addBoard(state, action) {
//     let newState = []

//     const newBoard = {
//         title: action.payload,
//         lists: []
//     }

//     newState = [...state, newBoard]

//     return newState;
// }

// function deleteList(state, action) {
//     let board = {}
//     let newState = []

//     board = state.filter(elem => elem.id === parseInt(action.payload.boardId))[0]
//     const lists = board.lists.filter(list => list.id !== parseInt(action.payload.listId))
//     board.lists = lists

//     Object.assign(newState, state)
//     const index = state.indexOf(board)
//     newState.pop(index)

//     newState[index] = board

//     return newState;
// }

// function deleteCard(state, action) {
//     let newState = []
//     let board = {}
//     let cards = []

//     board = state.filter(elem => elem.id === parseInt(action.payload.boardId))[0]
//     console.log(board, state)
//     let modifiedList = board.lists.find(list => list.id == action.payload.listId)
//     let otherLists = board.lists
//     cards = modifiedList.cards.filter(card => card.id != action.payload.cardId)
//     modifiedList.cards = cards
//     otherLists.map(list => {
//         if (list.id == action.payload.id)
//             return modifiedList
//         return list
//     })
//     board.lists = otherLists

//     newState = [
//         ...state, board
//     ]

//     return newState;
// }
const boardsReducer = (state = initialState, action) => {
    switch (action.type) {
        // case CONSTANTS.ADD_LIST:
        //     return addList(state, action)
        // case CONSTANTS.ADD_CARD:
        //     return addCard(state, action)
        // case CONSTANTS.ADD_BOARD:
        //     return addBoard(state, action)
        // case CONSTANTS.DELETE_BOARD:
        //     return deleteBoard(state, action)
        // case CONSTANTS.DELETE_LIST:
        //     return deleteList(state, action)
        // case CONSTANTS.DELETE_CARD:
        //     return deleteCard(state, action)
        case CONSTANTS.STORE_DATA:
            const newState = JSON.parse(action.payload)
            return newState;
        default:
            return state;
    }
}

export default boardsReducer;