import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Collection from './Collection.js';
import Board from './Board.js';
import './../styles/App.scss';

function App() {
  return (
    <div className='back'>
      <link 
        href="https://fonts.googleapis.com/icon?family=Material+Icons"
        rel="stylesheet"
      />
      <Router>
        <Switch>
          <Route path='/' exact component={Collection}></Route>
          <Route path='/board/:id' component={Board}></Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
