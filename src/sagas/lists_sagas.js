import { CONSTANTS, requestOptions } from './../actions/index';
import { takeEvery, all, put } from 'redux-saga/effects';
import { storeData } from './../actions/index';

function* manageList(action) {
    let data
    const { method, ...payload } = action.payload
    requestOptions.body = JSON.stringify(payload)

    yield fetch(process.env.REACT_APP_API_URL + method, requestOptions)
    .then(res => res.json())
    .then(json => data = json)

    yield put(storeData(data))
} 

function* manageListSaga() {
    yield takeEvery([CONSTANTS.ADD_LIST, CONSTANTS.EDIT_LIST, CONSTANTS.DELETE_LIST], manageList)
}

export default function* listSagas() {
    yield all([
        manageListSaga(),
    ])
}
