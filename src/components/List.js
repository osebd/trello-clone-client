import React from 'react';
import { Button } from '@material-ui/core';
import { Input } from '@material-ui/core';
import Card from './Card.js';
import { connect } from 'react-redux';

import store from './../store';
import { deleteList, editList } from './../actions';
import ActionButton from './ActionButton.js';
import './../styles/List.scss';

class List extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isEditing: false
        }
    }

    handleDelete = () => {
        store.dispatch(deleteList(this.props.boardId, this.props.listId))
    }

    handleEdit = (value) => {
        this.setState({ isEditing: value })
    }

    handleChange = (event) => {
        if(event.target.value !== '' 
            && event.target.value.length < 60 
                && event.target.value !== this.props.title)
                    this.setState({ value: event.target.value});
    };

    handleKeyDown = (e) => {
        if (e.keyCode == 13) {
            store.dispatch(editList(this.props.listId, this.state.value))
            this.handleEdit(false)
        } else if (e.keyCode == 27) {
            this.handleEdit(false)
        }
    }

    render() {
        return (
            <div className='list'>
                <Button className='btn' variant='contained' color='secondary' onClick={this.handleDelete}>Delete list</Button>
                <div className='list-header'>
                    <div className='title' onClick={() => this.handleEdit(true)}>{this.state.isEditing === false ? this.props.title
                        : <Input autoFocus 
                                onChange={this.handleChange} 
                                onKeyDown={this.handleKeyDown} 
                                placeholder="Insert new title" 
                                inputProps={{
                                    style: {
                                        textAlign: 'center',
                                        width: '380px'
                                    }
                                }}/>}</div>
                </div>
                <hr style={{width: '100%', textAlign:'left', marginLeft:'0'}}></hr>
                {
                    this.props.cards.map(card => <Card key={card.id} boardId={this.props.boardId} listId={this.props.listId} content={card.content} cardId={card.id} />)
                }
                <ActionButton boardId={this.props.boardId} listId={this.props.listId} buttonType='card' />
            </div>
        )
    }
}

const mapDispatchToProps = () => {

}

export default connect(null)(List);