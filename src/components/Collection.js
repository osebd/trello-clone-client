import React, { useState } from 'react';
import './../styles/Collection.scss';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import store from './../store/index.js';
import { deleteBoard, editBoard } from './../actions'
import ActionButton from './ActionButton.js';

const BoardCard = ({ title, id }) => {
    const [isEditing, setIsEditing] = useState(false);
    const [value, setValue] = useState(title);

    function handleRemove() {
        store.dispatch(deleteBoard(id));
    }

    function handleEdit(value) {
        setIsEditing(value)
    }

    const handleChange = (event) => {
        if(event.target.value !== '' 
            && event.target.value.length < 60 
                && event.target.value !== title)
        setValue(event.target.value);
    };

    function handleKeyDown(e) {
        if (e.keyCode == 13) {
            store.dispatch(editBoard(id, value))
            handleEdit(false)
        } else if (e.keyCode == 27) {
            handleEdit(false)
        }
    }

    return (
        <div className='board'>
            <div className='description' onClick={() => handleEdit(true)}>{isEditing === false ? title
                : <Input autoFocus onChange={handleChange} onKeyDown={handleKeyDown} placeholder="Insert new title"/>}
            </div>
            <div className='btn-wrp'>
                <Button className='btn' variant='contained' color='secondary'>
                    <Link className='link' to={`/board/${id}`}>Open</Link>
                </Button>
                <Button className='btn' variant='contained' color='primary' onClick={handleRemove}>Delete</Button>
            </div>
        </div>
    )
}

class Collection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isEditing: false
        }
    }
    render() {
        return (
            <div className='container'>
                <div className='title'>
                    Collection of boards
                </div>
                <div className='wrapper'>
                    {
                        Object.keys(this.props.boards).map(key => {
                            return <BoardCard key={key} title={this.props.boards[key].title} id={this.props.boards[key].id} />
                        })
                    }
                    <div style={{ width: '300px', display: 'flex', alignItems: 'center', marginLeft: '40px' }}>
                        <ActionButton buttonType='board' />
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    boards: state.boards
})


export default connect(mapStateToProps)(Collection);