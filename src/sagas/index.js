import { storeData, CONSTANTS } from './../actions/index';
import { put, takeEvery, all } from 'redux-saga/effects';
import listSagas from './lists_sagas';
import cardSagas from './card_sagas';
import boardSagas from './board_sagas';

function* initSaga() {
    let data
    yield fetch(process.env.REACT_APP_API_URL + 'get_boards')
    .then(res => res.json())
    .then(json => data = json)
    yield put(storeData(data))
}

function* watchInitSaga() {
    yield takeEvery(CONSTANTS.FETCH_DATA, initSaga)
}

export default function* rootSaga() {
    yield all([
        watchInitSaga(),
        boardSagas(),
        listSagas(),
        cardSagas()
    ])
}