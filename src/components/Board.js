import React from 'react';
import { connect } from "react-redux";
import store from './../store/index.js';
import List from './List';
import './../styles/Board.scss';
import ActionButton from './ActionButton';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router-dom';
import { editBoard } from './../actions/';
import Input from '@material-ui/core/Input';

class Board extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            board: null,
            isEditing: false,
            value: ''
        }
    }

    handleEdit = (value) => {
        this.setState({ isEditing: value });
    }

    handleChange = (event) => {
        if(event.target.value !== '' 
            && event.target.value.length < 60 
                && event.target.value !== this.state.board.title)
                    this.setState({ value: event.target.value});
    };

    handleKeyDown = (e) =>{
        if (e.keyCode == 13) {
            store.dispatch(editBoard(this.props.match.params.id, this.state.value))
            this.handleEdit(false)
        } else if (e.keyCode == 27) {
            this.handleEdit(false)
        }
    }

    static getDerivedStateFromProps(nextProps, prevState){
        if(nextProps.boards.length > 0){
            const newBoard = nextProps.boards.filter(board => board.id === parseInt(nextProps.match.params.id))
            if(newBoard !== prevState.board)
                return { board: newBoard[0] }
        }

        return null
    }

    render() {
        if(!this.state.board)
            return null


        const BackButton = withRouter(({ history }) => (
            <Button className='btn'
                variant='contained'
                color='secondary'
                style={{ margin: '10px', height: '40px' }}
                onClick={() => history.push("/")}
            >Back</Button>
        ))
        const { lists } = this.state.board
        return (
            <div className='wrp'>
                <BackButton />
                <h1 className='header' onClick={() => this.handleEdit(true)}>{this.state.isEditing === false ? this.state.board.title :
                <Input 
                    autoFocus 
                    onChange={this.handleChange} 
                    onKeyDown={this.handleKeyDown} 
                    placeholder="Insert new title"
                    className="input"
                    fullWidth={true}
                    inputProps={{
                        style: {
                            textAlign: 'center'
                        }
                    }}
                    />
                }</h1>
   
                <div className='board'>
                    {
                        lists.map(list => {
                            return <List key={list.id} boardId={this.props.match.params.id} listId={list.id} cards={list.cards} title={list.title} />
                        })
                    }
                    <ActionButton boardId={this.props.match.params.id} buttonType='list' />
                </div>
            </div>
        )
    }


}

const mapStateToProps = state => ({
    boards: state.boards
})

export default connect(mapStateToProps)(Board); 