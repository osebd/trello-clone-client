import React from 'react';
import Icon from "@material-ui/core/Icon";
import { Card, Button } from '@material-ui/core';
import TextArea from 'react-textarea-autosize';
import { connect } from 'react-redux';
import { addList, addCard, addBoard } from './../actions'

import './../styles/ActionButton.scss';

class ActionButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            formOpen: false
        }
    }

    openForm = () => {
        this.setState({ formOpen: true })
    }

    closeForm = () => {
        this.setState({ formOpen: false })
    }

    handleInputChange = (e) => {
        this.setState({
            text: e.target.value
        })
    }


    handleAdd = (type) => {
        const { dispatch, boardId, listId } = this.props;
        const { text } = this.state;

        if (text) {
            if(text === '')
                return
            switch (type) {
                case 'list':
                    dispatch(addList(boardId, text));
                    break;
                case 'board':
                    dispatch(addBoard(text));
                    break;
                case 'card':
                    dispatch(addCard(boardId, listId, text));
                    break;
                default:
                    break;
            }
            this.setState({ text: "" })
        }
    }
    
    renderForm = () => {
        const buttonType = this.props.buttonType;
        const placeHolders = {
            list: 'Enter a list title...',
            card: 'Enter a title for this card...',
            board: 'Enter a title for the board...'
        }

        const style = buttonType === 'card' ? {
            minWidth: 380,
            padding: '6px 8px 2px',
            minHeight: 85,
        } : {
                maxWidth: 300,
                padding: '6px 8px 2px',
                minHeight: 85,
                marginTop: '5%'
            }

    
        return <div>
            <Card style={style}>
                <TextArea
                    placeholder={placeHolders[buttonType]}
                    autoFocus
                    value={this.state.text}
                    onChange={this.handleInputChange}
                    style={{
                        resize: 'none',
                        width: '100%',
                        outline: 'none',
                        border: 'none',
                    }}
                />
            </Card>
            <div style={{
                display: 'flex',
                alignItems: 'center',
                minWidth: '300px',
                marginTop: '10px',
                marginBottom: '10px',
                justifyContent: 'space-between'
            }}>
                <Button variant='contained' color='primary' onMouseDown={() => this.handleAdd(buttonType)}>Add</Button>
                <Button variant='contained' color='secondary' onMouseDown={this.closeForm}>Cancel</Button>
            </div>
        </div>
    }

    renderAddButton = () => {
        const { buttonType } = this.props;

        const buttonText = `Add another ${buttonType}`;

        return (
            <div onClick={this.openForm} className="action-btn">
                <Icon>add</Icon>
                <p>{buttonText}</p>
            </div>
        )
    }

    render() {
        return this.state.formOpen ? this.renderForm() : this.renderAddButton();
    }
}

export default connect()(ActionButton);