import { CONSTANTS, RPC_METHODS } from './../actions';

export const addCard = (boardId, listId, cardContent) => {
    return {
        type: CONSTANTS.ADD_CARD,
        payload: { cardContent, listId, boardId, method: RPC_METHODS.ADD_CARD }
    };
}

export const deleteCard = (boardId, listId, cardId) => {
    return {
        type: CONSTANTS.DELETE_CARD,
        payload: { boardId, listId, cardId, method: RPC_METHODS.DELETE_CARD }
    }
}

export const editCard = (cardId, cardContent) => {
    return {
        type: CONSTANTS.EDIT_CARD,
        payload: { cardId, cardContent, method: RPC_METHODS.EDIT_CARD }
    }
}
