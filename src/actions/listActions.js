import { CONSTANTS, RPC_METHODS } from '../actions';

export const addList = (boardId, listTitle) => {
    return {
        type: CONSTANTS.ADD_LIST,
        payload: { listTitle, boardId, method: RPC_METHODS.ADD_LIST }
    }
}

export const deleteList = (boardId, listId) => {
    return {
        type: CONSTANTS.DELETE_LIST,
        payload: { boardId, listId, method: RPC_METHODS.DELETE_LIST }
    }
}

export const editList = (listId, listTitle) => {
    return {
        type: CONSTANTS.EDIT_LIST,
        payload: { listId, listTitle, method: RPC_METHODS.EDIT_LIST }
    }
}